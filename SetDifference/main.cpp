#include <list>
#include <iostream>

namespace mystd {
    template <typename It1, typename It2, typename Out>
    Out set_difference(It1 first1, It1 last1, It2 first2, It2 last2, Out out);
}

int main() {
    std::list<int> first, second, res;
    int first_size, second_size;

    std::cin >> first_size;
    for (size_t i = 0; i < first_size; ++i) {
        int elem;
        std::cin >> elem;
        first.push_back(elem);
    }

    std::cin >> second_size;
    for (size_t i = 0; i < second_size; ++i) {
        int elem;
        std::cin >> elem;
        second.push_back(elem);
    }
    res.resize(first_size);
    auto end = mystd::set_difference(first.begin(), first.end(),
                                     second.begin(), second.end(),
                                     res.begin());
    for (auto i = res.begin(); i != end; ++i) {
        std::cout << *i << ' ';
    }
}

template <typename It1, typename It2, typename Out>
Out mystd::set_difference(It1 first1, It1 last1,
                          It2 first2, It2 last2,
                          Out out) {
    while (first1 != last1) {
        if (first2 == last2) {
            return std::copy(first1, last1, out);
        }
        if (*first1 < *first2) {
            *out = *first1;
            ++out;
            ++first1;
        } else {
            if (!(*first2 < *first1)) {
                ++first1;
            }
            ++first2;
        }
    }
    return out;
}
