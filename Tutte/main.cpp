#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <map>
#include <ctime>

void ReadInput(std::vector<std::vector<int> > &graph, std::vector<int> &FixedVertexes) {
    int n, m;
    std::cin >> n >> m;
    FixedVertexes.resize(3);
    for (int i = 0; i < 3; ++i) {
        std::cin >> FixedVertexes[i];
        --FixedVertexes[i];
    }
    graph.resize(n);
    for (int i = 0; i < m; ++i) {
        int firstVertex, secondVertex;
        std::cin >> firstVertex >> secondVertex;
        --firstVertex;
        --secondVertex;
        graph[firstVertex].push_back(secondVertex);
        graph[secondVertex].push_back(firstVertex);
    }
}

bool CheckSimpleCycle(std::vector<std::vector<int> > &graph, std::vector<int> &FixedVertexes) {
    for (int i = 0; i < FixedVertexes.size() - 1; ++i) {
        bool HasEdge = false;
        int currentVertex = FixedVertexes[i];
        for (int j = 0; j < graph[currentVertex].size(); ++j) {
            if (graph[currentVertex][j] == FixedVertexes[i + 1]) {
                HasEdge = true;
            }
        }
        if (!HasEdge) {
            return false;
        }
    }
    for (int j = 0; j < graph[0].size(); ++j) {
        if (graph[FixedVertexes[0]][j] == FixedVertexes[FixedVertexes.size() - 1]) {
            return true;
        }
    }
    return false;
}

std::vector<double> SolveSystem(std::vector<std::vector<double> > system, 
                                std::vector<double> sum) {
    for (int i = 0; i < system.size(); ++i) {
        double maxValue = 0;
        int maxValuePos = i;
        for (int j = i; j < system.size(); ++j) {
            if (maxValue < system[j][i]) {
                maxValue = system[j][i];
                maxValuePos = j;
            }
        }
        std::swap(system[i], system[maxValuePos]);
        std::swap(sum[i], sum[maxValuePos]);
        for (int j = i + 1; j < system.size(); ++j) {
            double coef = system[j][i] / system[i][i];
            for (int k = i; k < system.size(); ++k) {
                system[j][k] -= system[i][k] * coef;
            }
            sum[j] -= sum[i] * coef;
        }
    }
    std::vector<double> ans(system.size());
    for (int i = system.size() - 1; i >= 0; --i) {
        double lineSum = sum[i];
        for (int j = system.size() - 1; j > i; --j) {
            lineSum -= system[i][j] * ans[j];
        }
        ans[i] = lineSum / system[i][i];
    }
    return ans;
}

std::vector<std::pair<double, double> > Solve(
     std::vector<std::vector<int> > &graph,
     std::vector<int> &FixedVertexes,
     std::vector < std::pair<double, double> > &FixedCoordinates) {
    std::vector < std::vector<int> > matrix(graph.size());
    for (int i = 0; i < graph.size(); ++i) {
        matrix[i].resize(graph.size());
    }

    for (int i = 0; i < graph.size(); ++i) {
        for (int j = 0; j < graph[i].size(); ++j) {
            matrix[i][graph[i][j]] = -1;
        }
        matrix[i][i] = graph[i].size();
    }

    int dimension = graph.size() - FixedCoordinates.size();
    std::map<int, std::pair<double, double> > fixedMap;
    for (int i = 0; i < FixedVertexes.size(); ++i) {
        fixedMap[FixedVertexes[i]] = FixedCoordinates[i];
    }
    std::vector<std::vector<double> > xSystem(dimension), ySystem(dimension);
    std::vector<double> xSum(dimension), ySum(dimension);
    int curRow = 0;
    for (int i = 0; i < matrix.size(); ++i) {
        int curCol = 0;
        if (fixedMap.find(i) != fixedMap.end()) {
            continue;
        }
        xSystem[curRow].resize(dimension);
        ySystem[curRow].resize(dimension);
        for (int j = 0; j < matrix.size(); ++j) {
            if (fixedMap.find(j) != fixedMap.end()) {
                xSum[curRow] -= fixedMap[j].first * matrix[i][j];
                ySum[curRow] -= fixedMap[j].second * matrix[i][j];
                continue;
            }
            xSystem[curRow][curCol] = matrix[i][j];
            ySystem[curRow][curCol] = matrix[i][j];
            ++curCol;
        }
        ++curRow;
    }
    
    std::vector<double> xCoords = SolveSystem(xSystem, xSum);
    std::vector<double> yCoords = SolveSystem(ySystem, ySum);

    int curVertex = 0;
    std::vector<std::pair<double, double> > ans;
    for (int i = 0; i < graph.size(); ++i) {
        if (fixedMap.find(i) != fixedMap.end()) {
            ans.push_back(fixedMap[i]);
        } else {
            ans.push_back(std::make_pair(xCoords[curVertex], yCoords[curVertex]));
            curVertex++;
        }
    }
    return ans;
}

void PrintAns(std::vector<std::pair<double, double> > &ans) {
    std::cout.precision(10);
    for (int i = 0; i < ans.size(); ++i) {
        std::cout << ans[i].first << ' ' << ans[i].second << '\n';
    }
}

int main() {
    std::vector<std::vector<int> > graph;
    std::vector<std::pair<double, double> > ans;
    std::vector<int> FixedVertexes;
    ReadInput(graph, FixedVertexes);
    if (!CheckSimpleCycle(graph, FixedVertexes)) {
        return 0;
    } else {
        std::vector<std::pair<double, double> > FixedCoordinates = {
            std::make_pair(0, 0),
            std::make_pair(1, 0),
            std::make_pair(1, 1)
        };
        ans = Solve(graph, FixedVertexes, FixedCoordinates);
    }
    PrintAns(ans);
}
