#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>

void ReadGraph(std::vector<std::pair<int, int> >& edges, std::vector<int64_t>& vertex_mass) {
    int vertex_number, edges_number;
    std::cin >> vertex_number >> edges_number;
    vertex_mass.reserve(vertex_number);
    edges.reserve(edges_number);
    for (int i = 0; i < vertex_number; ++i) {
        int64_t mass;
        std::cin >> mass;
        vertex_mass.push_back(mass);
    }
    for (int i = 0; i < edges_number; ++i) {
        int edge_begin, edge_end;
        std::cin >> edge_begin >> edge_end;
        edges.push_back({ edge_begin - 1, edge_end - 1 });
    }
}

std::vector<int> FindCoveringSet(const std::vector<std::pair<int, int> >& edges,
                                 const std::vector<int64_t>& vertex_weight) {
    std::vector<int64_t> current_weight(vertex_weight.size(), 0);
    for (auto edge : edges) {
        auto first_diff = vertex_weight[edge.first] - current_weight[edge.first];
        auto second_diff = vertex_weight[edge.second] - current_weight[edge.second];
        if (first_diff > 0 && second_diff > 0) {
            auto add = std::min(first_diff, second_diff);
            current_weight[edge.first] += add;
            current_weight[edge.second] += add;
        }
    }
    std::vector<int> covering_set;
    for (int i = 0; i < vertex_weight.size(); ++i) {
        if (vertex_weight[i] == current_weight[i]) {
            covering_set.push_back(i);
        }
    }
    return covering_set;
}

void PrintVertexSet(const std::vector<int>& set) {
    for (auto vertex_index : set) {
        std::cout << vertex_index + 1 << ' ';
    }
}

int main() {
    std::vector<int64_t> vertex_mass;
    std::vector<std::pair<int, int> > edges;
    ReadGraph(edges, vertex_mass);
    std::vector<int> covering_set = FindCoveringSet(edges, vertex_mass);
    PrintVertexSet(covering_set);
}
