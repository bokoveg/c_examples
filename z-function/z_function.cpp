#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

std::vector<size_t> CalculateZFunction(std::string &line) {
    std::vector<size_t> z_function(line.length());
    size_t left = 0;
    size_t right = 0;
    z_function[0] = line.length();
    for (size_t index = 1; index < line.length(); ++index) {
        if (index < right) {
            z_function[index] = std::min(z_function[index - left], right - index);
        }
        while (index + z_function[index] < line.length() &&
               line[index + z_function[index]] == line[z_function[index]]) {
            ++z_function[index];
        }
        if (index + z_function[index] > right) {
            right = index + z_function[index];
            left = index;
        }
    }
    return z_function;
}

void PrintVector(std::vector<size_t> &vec) {
    for (auto i : vec) {
        std::cout << i << ' ';
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::string line;
    std::cin >> line;
    auto ans = CalculateZFunction(line);
    PrintVector(ans);
}
