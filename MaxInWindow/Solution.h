#pragma once

#include "ExtendedQueue.h"

void ReadInput(std::vector<int> &elements, std::vector<char> &operations) {
	int elementsNumber;
	std::cin >> elementsNumber;
	elements.resize(elementsNumber);
	for (int i = 0; i < elementsNumber; ++i) {
		std::cin >> elements[i];
	}

	int operationsNumber;
	std::cin >> operationsNumber;
	operations.resize(operationsNumber);
	for (int i = 0; i < operationsNumber; ++i) {
		std::cin >> operations[i];
	}
}

void Solve(std::vector<int> &elements,
	std::vector<char> &operations,
	std::vector<int> &answer) {
	ExtendedQueue queue;
	int left = 0, right = 0;
	queue.push(elements[0]);
	for (int i = 0; i < operations.size(); ++i) {
		if (operations[i] == 'R') {
			++right;
			queue.push(elements[right]);
		} else {
			++left;
			queue.pop();
		}
		answer.push_back(queue.getMax());
	}
}

void PrintAnswer(std::vector<int> &answer) {
	for (int i = 0; i < answer.size(); ++i) {
		std::cout << answer[i] << ' ';
	}
}