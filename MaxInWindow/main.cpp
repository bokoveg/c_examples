#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <stack>

#include "StressTester.h"


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::vector<int> elements;
    std::vector<char> operations;

    ReadInput(elements, operations);

    std::vector<int> answer;
    Solve(elements, operations, answer);

    PrintAnswer(answer);
}
