#pragma once
#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <stack>

class ExtendedStack {
 public:
	ExtendedStack() {
	}

	void push(int element) {
		data.push(element);
		int curMax = element;
		if (maxs.size() > 0) {
			curMax = maxs.top();
		}
		maxs.push(std::max(curMax, element));
	}

	int pop() {
		maxs.pop();
		int lastElement = data.top();
		data.pop();
		return lastElement;
	}

	int top() {
		return data.top();
	}

	int getMax() {
		return maxs.top();
	}

	int size() {
		return data.size();
	}

 private:
	std::stack<int> data, maxs;
};
