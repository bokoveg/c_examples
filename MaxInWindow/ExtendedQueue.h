#pragma once
#include "ExtendedStack.h"

class ExtendedQueue {
 public:
	ExtendedQueue() {
	}

	void push(int element) {
		tail.push(element);
	}

	void pop() {
		if (head.size() == 0) {
			while (tail.size() > 0) {
				head.push(tail.top());
				tail.pop();
			}
		}
		head.pop();
	}

	int getMax() {
		if (tail.size() == 0) {
			return head.getMax();
		}
		if (head.size() == 0) {
			return tail.getMax();
		}
		return std::max(tail.getMax(), head.getMax());
	}

 private:
	ExtendedStack tail, head;
};
