#include "Solution.h"

void GenerateRandomTest(std::vector<int> &elements, 
						std::vector<char> &operations) {
	
	int n = rand();
	elements.clear();
	elements.resize(n);
	for (int i = 0; i < n; ++i) {
		int pos = 1;
		if (rand() % 2 == 1) {
			pos = -1;
		}
		elements[i] = (rand() * rand()) * pos;
	}
	
	int m = (rand() * rand()) % (2 * n - 2);
	operations.clear();
	operations.resize(m);
	int rightMoves = 0;
	int counter = 0;
	for (int i = 0; i < m; ++i) {
		if (rightMoves == n - 1) {
			for (int j = 0; j < m - i; ++j) {
				operations[i + j] = 'L';
			}
			break;
		}
		if (counter == 0) {
			operations[i] = 'R';
			++rightMoves;
			++counter;
		} else {
			int x = rand() % 2;
			if (x == 0) {
				operations[i] = 'L';
				--counter;
			} else {
				operations[i] = 'R';
				++rightMoves;
				++counter;
			}
		}
	}
}

int FindMaxElement(std::vector<int> elements, int left, int right) {
	int x = elements[right];
	for (int i = left; i < right; ++i) {
		x = std::max(elements[i], x);
	}
	return x;
}

void ReferenceSolve(std::vector<int> &elements, 
					std::vector<char> &operations, 
					std::vector<int> &answer) {
	int left = 0;
	int right = 0;
	for (int i = 0; i < operations.size(); ++i) {
		if (operations[i] == 'L') {
			++left;
		} else {
			++right;
		}
		answer.push_back(FindMaxElement(elements, left, right));
	}
}

bool Check(std::vector<int> &reference, std::vector<int> &algoAnswer) {
	if (reference.size() != algoAnswer.size()) {
		return false;
	}
	for (int i = 0; i < reference.size(); ++i) {
		if (reference[i] != algoAnswer[i]) {
			return false;
		}
	}
	return true;
}

void StartStressTesting(int iterationsNumber) {
	for (int i = 0; i < iterationsNumber; ++i) {
		std::vector<int> elements, reference, answer;
		std::vector<char> operations;
		GenerateRandomTest(elements, operations);
		Solve(elements, operations, answer);
		ReferenceSolve(elements, operations, reference);
		if (!Check(reference, answer)) {
			std::cout << "Found error in solution:\n n = " << elements.size()
				<< "\n m = " << operations.size() << "\n ";
			for (int i = 0; i < elements.size(); ++i) {
				std::cout << elements[i] << ' ';

			}
			std::cout << "\n";
			for (int i = 0; i < operations.size(); ++i) {
				std::cout << operations[i] << ' ';
			}
			std::cout << "\n Found:\n";
			PrintAnswer(answer);
			std::cout << "\n Expected:\n";
			PrintAnswer(reference);
			std::cout << "\n";
			break;
		}
		std::cout << "Random test " << i << " passed.\n";
	}
}
