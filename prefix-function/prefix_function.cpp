#include <iostream>
#include <vector>
#include <string>

std::vector<size_t> CalculatePrefixFunction(std::string &line) {
    std::vector<size_t> prefix_function(line.length());
    prefix_function[0] = 0;
    for (size_t i = 1; i < line.length(); ++i) {
        size_t cur = prefix_function[i - 1];
        while (cur > 0 && line[cur] != line[i]) {
            cur = prefix_function[cur - 1];
        }
        if (line[cur] == line[i]) {
            prefix_function[i] = cur + 1;
        }
    }
    return prefix_function;
}

void PrintVector(std::vector<size_t> &vec) {
    for (auto i : vec) {
        std::cout << i << ' ';
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::string line;
    std::cin >> line;
    std::vector<size_t> prefix_function = CalculatePrefixFunction(line);
    PrintVector(prefix_function);
    return 0;
}
