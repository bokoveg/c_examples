#include <iostream>
#include <vector>
#include <ctime>
#include <queue>

struct Node {
    Node *left, *right;
    int key;
    ~Node () {
        delete left;
        delete right;
    }
};

std::vector<int> ReadInput() {
    size_t TreeSize;
    std::cin >> TreeSize;

    std::vector<int> tree(TreeSize);
    for (size_t i = 0; i < tree.size(); ++i) {
        std::cin >> tree[i];
    }
    return tree;
}

int binsearch(std::vector<int> &keys, int left, int right, int elem) {
    if (keys[left] >= elem) {
        return left;
    }
    while (right - left > 1) {
        int mid = (left + right) / 2;
        if (keys[mid] < elem) {
            left = mid;
        } else {
            right = mid;
        }
    }
    return right;
}

Node* BuildByPreorder(std::vector<int> &keys, int minIndex, int maxIndex) {
    Node *newNode = new Node;
    newNode->key = keys[minIndex];
    if (maxIndex - minIndex == 1) {
        newNode->left = nullptr;
        newNode->right = nullptr;
        return newNode;
    }
    int curIndex = binsearch(keys, minIndex + 1, maxIndex, newNode->key);
    if (curIndex > minIndex + 1) {
        newNode->left = BuildByPreorder(keys, minIndex + 1, curIndex);
    } else {
        newNode->left = nullptr;
    }
    if (curIndex == maxIndex) {
        newNode->right = nullptr;
    } else {
        newNode->right = BuildByPreorder(keys, curIndex, maxIndex);
    }
    return newNode;
}

void BuildInOrder(Node* root, std::vector<int> &ans) {
    if (root->left != nullptr) {
        BuildInOrder(root->left, ans);
    }
    ans.push_back(root->key);
    if (root->right != nullptr) {
        BuildInOrder(root->right, ans);
    }
}

void BuildPostOrder(Node* root, std::vector<int> &ans) {
    if (root->left != nullptr) {
        BuildPostOrder(root->left, ans);
    }
    if (root->right != nullptr) {
        BuildPostOrder(root->right, ans);
    }
    ans.push_back(root->key);
}

void BuildPreOrder(Node* root, std::vector<int> &ans) {
    ans.push_back(root->key);
    if (root->left != nullptr) {
        BuildPreOrder(root->left, ans);
    }
    if (root->right != nullptr) {
        BuildPreOrder(root->right, ans);
    }
}

void PrintArray(std::vector<int> &vec) {
    for (auto i : vec) {
        std::cout << i << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::vector<int> preorder = ReadInput();
    Node* root = BuildByPreorder(preorder, 0, preorder.size());
    std::vector<int> inorder, postorder;
    BuildInOrder(root, inorder);
    BuildPostOrder(root, postorder);
    delete root;
    PrintArray(postorder);
    PrintArray(inorder);
    return 0;
}
