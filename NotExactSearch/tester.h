//
// Created by egor on 08.03.17.
//
#include <ctime>
#include <string>
#include <vector>

std::pair<std::string, std::string> GenerateLines(size_t max_letter, size_t pattern_length, size_t text_length) {
    std::string pattern = "", text = "";
    for (size_t i = 0; i < pattern_length; ++i) {
        pattern += (char) ('a' + rand() % max_letter);
    }
    for (size_t i = 0; i < text_length; ++i) {
        text += (char) ('a' + rand() % max_letter);
    }
    return std::make_pair(pattern, text);
}

std::vector<size_t> BruteSolve(std::string &pattern, std::string &text) {
    std::vector<size_t> ans;
    for (size_t i = 0; i < text.length() - pattern.length() + 1; ++i) {
        size_t errors_num = 0;
        for (size_t j = i; j < i + pattern.length(); ++j) {
            if (pattern[j - i] != text[j]) {
                errors_num++;
            }
            if (errors_num > 1) {
                break;
            }
        }
        if (errors_num <= 1) {
            ans.push_back(i);
        }
    }
    return ans;
}
