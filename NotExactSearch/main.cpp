/*#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>

#include "tester.h" //a

std::vector<size_t> CalculateZFunction(std::string &line) {
    std::vector<size_t> z_function(line.length());
    size_t left = 0;
    size_t right = 0;
    z_function[0] = line.length();
    for (size_t index = 1; index < line.length(); ++index) {
        if (index < right) {
            z_function[index] = std::min(z_function[index - left], right - index);
        }
        while (index + z_function[index] < line.length() &&
               line[index + z_function[index]] == line[z_function[index]]) {
            ++z_function[index];
        }
        if (index + z_function[index] > right) {
            right = index + z_function[index];
            left = index;
        }
    }
    return z_function;
}

void PrintVector(std::vector<size_t> &vec) {
    std::cout << vec.size() << '\n';
    for (auto i : vec) {
        std::cout << i + 1 << ' ';
    }
}

std::vector<size_t> FindPattern(std::string &pattern, std::string &text) {
    std::vector<size_t> ans;
    std::string line = pattern + "#" + text;
    std::reverse(text.begin(), text.end());
    std::reverse(pattern.begin(), pattern.end());
    std::string reversed_line = pattern + '#' + text;
    auto z_function = CalculateZFunction(line);
    auto reversed_z_function = CalculateZFunction(reversed_line);
    for (size_t ind = 0; ind < text.size(); ++ind) {
        if (text.length() - ind < pattern.length()) {
            break;
        }
        if (z_function[pattern.length() + ind + 1] == pattern.length()) {
            ans.push_back(ind);
        } else {
            if (z_function[pattern.length() + ind + 1] +
                reversed_z_function[1 + text.length() - ind] == pattern.length() - 1) {
                // std::cout << z_function[pattern.length() + ind + 1] << ' ' << reversed_z_function[1 + text.length() - ind] << '\n';
                ans.push_back(ind);
            }
        }
    }
    return ans;
}

/* int main() {
    // srand(time(NULL));
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::string pattern, text;

    for (int test_index = 0; test_index < 10; ++test_index) {
        auto lines = GenerateLines(5, 30, 1000000);
        pattern = lines.first;
        text = lines.second;


        auto brute_ans = BruteSolve(pattern, text);
        auto ans = FindPattern(pattern, text);

        if (ans != brute_ans) {
            std::cout << pattern << '\n' << text << '\n';
            PrintVector(ans);
            std::cout << '\n';
            PrintVector(brute_ans);
            std::cout << '\n';
            exit(0);
        }
    }
    /*std::cin >> pattern >> text;
    auto ans = FindPattern(pattern, text);
    PrintVector(ans);
}*/
