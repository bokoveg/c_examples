#include "tester.h"
#include <stdio.h>
#include <iostream>
#include <vector>

int main() {
    freopen("10.txt", "w", stdout);
    auto lines = GenerateLines(3, 10000, 1000000);
    std::cout << lines.first << '\n' << lines.second << '\n';
}

