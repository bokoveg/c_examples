#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

class DSU {
public:
    explicit DSU(size_t vertex_number) {
        for (size_t i = 0; i < vertex_number; ++i) {
            set_index_.push_back(i);
            set_info_.push_back(i);
        }
    }

    size_t find_set(size_t index) {
        if (set_index_[index] == index) {
            return index;
        }
        return set_index_[index] = find_set(set_index_[index]);
    }

    void union_sets(size_t first, size_t second) {
        auto first_set = find_set(first);
        auto second_set = find_set(second);
        if (first_set != second_set) {
            set_index_[second_set] = first_set;
        }
    }

private:
    std::vector<size_t> set_index_;
    std::vector<size_t> set_info_;
};

int main() {
    int unit_number, requests_number;
    std::cin >> unit_number >> requests_number;
    std::string line;
    std::getline(std::cin, line);
    DSU dsu(unit_number);
    for (int it = 0; it < requests_number; ++it) {
        std::getline(std::cin, line);
        size_t space_pos = line.find(" ");
        if (space_pos == std::string::npos) {
            int unit_index = std::stoi(line);
            std::cout << dsu.find_set(unit_index - 1) + 1 << "\n";
        } else {
            std::string first_part = line.substr(0, space_pos);
            std::string second_part = line.substr(space_pos);
            int first_unit_index = std::stoi(first_part) - 1;
            int second_unit_index = std::stoi(second_part) - 1;
            if (dsu.find_set(second_unit_index) == second_unit_index &&
                second_unit_index != first_unit_index && first_unit_index <= unit_number &&
                second_unit_index <= unit_number &&
                dsu.find_set(first_unit_index) != second_unit_index) {
                dsu.union_sets(first_unit_index, second_unit_index);
                std::cout << "1\n";
            } else {
                std::cout << "0\n";
            }
        }
    }
}
