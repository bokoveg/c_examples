#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <memory>
#include <ctime>
#include <list>
#include <random>

struct Segment {
    int left, right;
    Segment(int left, int right) : left(left), right(right) {};
    Segment() {
        left = right = 0;
    }
    bool operator < (const Segment &rhs) const {
        return left < rhs.left;
    }
    bool operator == (const Segment &rhs) const {
        return left == rhs.left && right == rhs.right;
    }
};

struct Item {
    Segment key;
    int prior;
    std::shared_ptr<Item> left, right;
    Item(Segment key, int prior) : key(key), prior(prior), left(nullptr), right(nullptr) {};
    std::list<Item>::iterator index;
};

void split(std::shared_ptr<Item> tree,
           Segment key,
           std::shared_ptr<Item> &left,
           std::shared_ptr<Item> &right) {
    if (!tree) {
        left = nullptr;
        right = nullptr;
    } else {
        if (key < tree->key) {
            split(tree->left, key, left, tree->left);
            right = tree;
        } else {
            split(tree->right, key, tree->right, right);
            left = tree;
        }
    }
}

void insert(std::shared_ptr<Item> &tree, std::shared_ptr<Item> value_pointer) {
    if (!tree) {
        tree = value_pointer;
    } else {
        if (tree->prior > value_pointer->prior) {
            split(tree, value_pointer->key, value_pointer->left, value_pointer->right);
            tree = value_pointer;
        } else {
            if (value_pointer->key < tree->key) {
                insert(tree->left, value_pointer);
            } else {
                insert(tree->right, value_pointer);
            }
        }
    }
}

void merge(std::shared_ptr<Item> &tree, std::shared_ptr<Item> left, std::shared_ptr<Item> right) {
    if (!left) {
        tree = right;
        return;
    }
    if (!right) {
        tree = left;
        return;
    }
    if (left->prior > right->prior) {
        merge(left->right, left->right, right);
        tree = left;
    } else {
        merge(right->left, left, right->left);
        tree = right;
    }
}

bool erase(std::shared_ptr<Item> &tree, Segment &key) {
    if (tree == nullptr) {
        return false;
    }
    if (tree->key == key) {
        merge(tree, tree->left, tree->right);
        return true;
    } else {
        if (tree == nullptr) {
            return false;
        }
        if (key < tree->key) {
            return erase(tree->left, key);
        } else {
            return erase(tree->right, key);
        }
    }
}

struct Event {
    bool start;
    Segment seg;
    int height;

    bool operator < (const Event &rhs) const {
        if (height != rhs.height) {
            return height < rhs.height;
        } else {
            if (seg < rhs.seg || rhs.seg < seg) {
                return seg < rhs.seg;
            } else {
                if (start && !rhs.start) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
};

Segment find_max(std::shared_ptr<Item> tree) {
    auto cur = tree;
    while (cur->right != nullptr) {
        cur = cur->right;
    }
    return cur->key;
}

int main() {
    std::vector<Event> events;
    int rectangle_number;
    std::cin >> rectangle_number;
    std::list<Item> nodes;
    std::shared_ptr<Item> tree = nullptr;
    for (int it = 0; it < rectangle_number; ++it) {
        int x_first, y_first, x_second, y_second;
        std::cin >> x_first >> y_first >> x_second >> y_second;
        if (x_first > x_second) {
            std::swap(x_first, x_second);
        }
        if (y_first > y_second) {
            std::swap(y_first, y_second);
        }
        Event start_event, end_event;
        start_event.height = y_first;
        start_event.seg = Segment(x_first, x_second);
        start_event.start = true;

        end_event.height = y_second;
        end_event.seg = Segment(x_first, x_second);
        end_event.start = false;

        events.push_back(start_event);
        events.push_back(end_event);
    }
    int ans = 0;
    std::mt19937 generator;
    std::sort(events.begin(), events.end());
    for (size_t it = 0; it < events.size(); ++it) {
        if (events[it].start) {
            std::shared_ptr<Item> left, right;
            split(tree, events[it].seg, left, right);
            int key_right;
            if (left == nullptr) {
                merge(tree, left, right);
                std::uniform_int_distribution<int> dist;
                auto prior = dist(generator);
                std::shared_ptr<Item> new_item(new Item(events[it].seg, prior));
                insert(tree, new_item);
            } else {
                key_right = find_max(left).right;
                merge(tree, left, right);
                if (!(key_right > events[it].seg.right)) {
                    std::uniform_int_distribution<int> dist;
                    auto prior = dist(generator);
                    std::shared_ptr<Item> new_item(new Item(events[it].seg, prior));
                    insert(tree, new_item);
                }
            }
        } else {
            if (erase(tree, events[it].seg)) {
                ++ans;
            }
        }
    }
    std::cout << ans;
}
