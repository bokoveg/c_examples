#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>

class Matrix {
 public:
    Matrix() {
        val_ = 0;
        shape_ = std::make_pair(1, 1);
    }

    Matrix(double val) {
        val_ = val;
        shape_ = std::make_pair(1, 1);
    }

    Matrix(const std::vector<std::vector<Matrix> > &data) {
        data_ = data;
        if (data.size() > 0) {
            size_ = std::make_pair(data.size(), data[0].size());
        }

        shape_ = size_;
        int shape = 1;
        while (shape < std::max(size_.first, size_.second)) {
            shape = shape << 1;
        }
        Reshape(shape, shape);
    }

    Matrix(const std::vector<std::vector<double> > &data) {
        if (data.size() > 0) {
            size_ = std::make_pair(data.size(), data[0].size());
        }
        data_.resize(data.size());
        for (int i = 0; i < data.size(); ++i) {
            data_[i].resize(data[i].size());
            for (int j = 0; j < data[i].size(); ++j) {
                data_[i][j] = Matrix(data[i][j]);
            }
        }

        shape_ = size_;
        int shape = 1;
        while (shape < std::max(size_.first, size_.second)) {
            shape = shape << 1;
        }
        Reshape(shape, shape);
    }

    Matrix operator * (const Matrix &rhs) {
        if (size_.second != rhs.size_.first) {
            throw std::length_error("Operator*: matrixes must have correct size!");
        }
        std::vector<std::vector<double> > Ans(size_.first);
        for (int i = 0; i < size_.first; ++i) {
            Ans[i].resize(rhs.size_.second);
            for (int j = 0; j < rhs.size_.second; ++j) {
                for (int k = 0; k < size_.second; ++k) {
                    Ans[i][j] += data_[i][k].GetVal() * rhs.data_[k][j].GetVal();
                }
            }
        }
        return Matrix(Ans);
    }

    Matrix operator + (const Matrix &rhs) {
        if (size_ != rhs.size_) {
            throw std::length_error("Operator+: matrixes must have same size!");
        }
        std::vector<std::vector<double> > Ans(size_.first);
        for (int i = 0; i < size_.first; ++i) {
            Ans[i].resize(size_.second);
            for (int j = 0; j < size_.second; ++j) {
                Ans[i][j] = data_[i][j].GetVal() + rhs.data_[i][j].GetVal();
            }
        }
        return Matrix(Ans);
    }

    Matrix operator - (const Matrix &rhs) {
        std::vector<std::vector<double> > Ans(size_.first);
        for (int i = 0; i < size_.first; ++i) {
            Ans[i].resize(size_.second);
            for (int j = 0; j < size_.second; ++j) {
                Ans[i][j] = data_[i][j].GetVal() - rhs.data_[i][j].GetVal();
            }
        }
        return Matrix(Ans);
    }

    Matrix GetSubMatrix(int xBegin, int xEnd, int yBegin, int yEnd) const {
        std::vector<std::vector<Matrix> > res(xEnd - xBegin);
        for (int i = 0; i < xEnd - xBegin; ++i) {
            res[i].resize(yEnd - yBegin);
            for (int j = 0; j < yEnd - yBegin; ++j) {
                res[i][j] = data_[xBegin + i][yBegin + j];
            }
        }
        return Matrix(res);
    }

    void Split(std::vector<Matrix> &res) const {
        res.clear();
        res.resize(4);
        res[0] = GetSubMatrix(0, shape_.first / 2, 0, shape_.second / 2);
        res[1] = GetSubMatrix(0, shape_.first / 2, shape_.second / 2, shape_.second);
        res[2] = GetSubMatrix(0, shape_.first / 2, 0, shape_.second / 2);
        res[3] = GetSubMatrix(shape_.first / 2, shape_.second, shape_.second / 2, shape_.second);
    }

    Matrix Merge(Matrix a11, Matrix a12, Matrix a21, Matrix a22) {
        int size = a11.shape_.first;

        std::vector<std::vector<Matrix> > ans(2 * size);
        if (size == 1) {
            ans[0].push_back(a11.GetVal());
            ans[0].push_back(a12.GetVal());
            ans[1].push_back(a21.GetVal());
            ans[1].push_back(a22.GetVal());
            return Matrix(ans);
        }
        for (int i = 0; i < ans.size(); ++i) {
            ans[i].resize(2 * size);
            for (int j = 0; j < ans[i].size(); ++j) {
                if (i < size && j < size) {
                    ans[i][j] = a11.data_[i][j];
                }
                if (i >= size && j < size) {
                    ans[i][j] = a21.data_[i - size][j];
                }
                if (i < size && j >= size) {
                    ans[i][j] = a12.data_[i][j - size];
                }
                if (i >= size && j >= size) {
                    ans[i][j] = a22.data_[i - size][j - size];
                }
            }
        }
        return Matrix(ans);
    }

    Matrix operator ^ (const Matrix &rhs) {
        if (shape_.first <= kMinSize) {
            return *this * rhs;
        }
        std::vector<Matrix> lparts, rparts;
        Split(lparts);
        rhs.Split(rparts);
        Matrix a11 = lparts[0], a12 = lparts[1], a21 = lparts[2], a22 = lparts[3];
        Matrix b11 = rparts[0], b12 = rparts[1], b21 = rparts[2], b22 = rparts[3];
        Matrix s1, s2, s3, s4, s5, s6, s7, s8, p1, p2, p3, p4, p5, p6, p7, t1, t2, c11, c12, c21, c22;

        s1 = a21 + a22;
        s2 = s1 - a11;
        s3 = a11 - a21;
        s4 = a12 - s2;
        s5 = b12 - b11;
        s6 = b22 - s5;
        s7 = b22 - b12;
        s8 = s6 - b21;

        p1 = s2 ^ s6;
        p2 = a11 ^ b11;
        p3 = a12 ^ b21;
        p4 = s3 ^ s7;
        p5 = s1 ^ s5;
        p6 = s4 ^ b22;
        p7 = a22 ^ s8;

        t1 = p1 + p2;
        t2 = t1 + p4;

        c11 = p2 + p3;
        c12 = t1 + p5 + p6;
        c21 = t2 - p7;
        c22 = t2 + p5;

        Matrix Ans = Merge(c11, c12, c21, c22);
        Ans.Reshape(size_.first, rhs.size_.second);
        return Ans;
    }

    void Reshape(int x, int y) {
        if (x < shape_.first) {
            for (int i = 0; i < shape_.first - x; ++i) {
                data_.pop_back();
            }
        } else {
            std::vector<Matrix> zeros(shape_.second);
            for (int i = 0; i < x - shape_.first; ++i) {
                data_.push_back(zeros);
            }
        }

        if (y < shape_.second) {
            for (int i = 0; i < data_.size(); ++i) {
                for (int j = 0; j < shape_.second - y; ++j) {
                    data_[i].pop_back();
                }
            }
        } else {
            for (int i = 0; i < data_.size(); ++i) {
                for (int j = 0; j < y - shape_.second; ++j) {
                    data_[i].push_back(Matrix(0));
                }
            }
        }
        shape_ = std::make_pair(x, y);
        size_ = std::make_pair(std::min(x, size_.first), std::min(y, size_.second));
    }

    void print() const {
        for (int i = 0; i < size_.first; ++i) {
            for (int j = 0; j < size_.second; ++j) {
                std::cout << data_[i][j].GetVal() << ' ';
            }
            std::cout << '\n';
        }
    }

    std::pair<int, int> GetShape() {
        return shape_;
    }

 private:
     double GetVal() const {
         return val_;
     }

     int kMinSize = 128;

     std::vector<std::vector<Matrix> > data_; 

     std::pair<int, int> shape_;

     std::pair<int, int> size_;

     double val_; 
};

std::vector<std::vector<double> > ReadMatrix(int n, int m) {
    std::vector<std::vector<double> > res(n);
    for (int i = 0; i < n; ++i) {
        res[i].resize(m);
        for (int j = 0; j < m; ++j) {
            std::cin >> res[i][j];
        }
    }
    return res;
}

int main() {
    int n;
    std::cin >> n;
    std::vector<std::vector<double> > m1, m2;
    m1 = ReadMatrix(n, n);
    m2 = ReadMatrix(n, n);
    Matrix res = Matrix(m1) ^ Matrix(m2);
    res.print();
}
