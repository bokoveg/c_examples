#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <string>
#include <ctime>

class HashFunction {
public:
    static HashFunction GenerateRandomHashFunction(std::mt19937& generator,
                                                   int64_t max_value) {
        std::uniform_int_distribution<int> first_coefficent_distribution(1, kPrimeModule - 1);
        std::uniform_int_distribution<int> second_coefficent_distribution(0, kPrimeModule - 1);
        auto first_coefficent = first_coefficent_distribution(generator);
        auto second_coefficent = second_coefficent_distribution(generator);
        return HashFunction(first_coefficent, second_coefficent, max_value);
    }

    HashFunction(int64_t first_coefficent, int64_t second_coefficent, int64_t max_value) :
        first_coefficent_(first_coefficent),
        second_coefficent_(second_coefficent),
        max_value_(max_value) {};
    
    int64_t operator() (int key) const {
        return ((key + kShift) * first_coefficent_ + second_coefficent_) %
               kPrimeModule  % max_value_;
    }

    HashFunction() : first_coefficent_(0), second_coefficent_(0), max_value_(0) {}

private:
    int64_t first_coefficent_, second_coefficent_, max_value_;
    static const int64_t kPrimeModule = 2000000011;
    static const int64_t kShift = 1000000000;
};

template <typename T>
struct Element {
    T value;
    bool initialized; 
};


template <typename T, class HashFunction>
class InnerHash {
public:
    InnerHash() {}

    void Initialize(const std::vector<T>& numbers, std::mt19937& generator) {
        hash_function_ = FindHashFunction(numbers, generator, numbers.size() * numbers.size());
        FillHashFunction(numbers, hash_function_, numbers.size() * numbers.size(), hash_values_);
    }

    bool Contains(const T number) const {
        if (hash_values_.size() == 0) {
            return false;
        }
        auto hash_value = hash_function_(number);
        return hash_values_[hash_value].initialized && hash_values_[hash_value].value == number;
    }

private:
    static HashFunction FindHashFunction(const std::vector<T>& numbers,
                                         std::mt19937& generator,
                                         size_t size) {
        HashFunction hash_function;
        do {
            hash_function = HashFunction::GenerateRandomHashFunction(generator, size);
        } while (!CheckHashCorrectness(numbers, hash_function, size));
        return hash_function;
    }

    static bool CheckHashCorrectness(const std::vector<T>& numbers,
                                     HashFunction& hash_function,
                                     size_t size) {
        std::vector<bool> hash_values(size, false);
        for (auto element : numbers) {
            if (hash_values[hash_function(element)]) {
                return false;
            } else {
                hash_values[hash_function(element)] = true;
            }
        }
        return true;
    }

    static void FillHashFunction(const std::vector<T>& numbers,
                                 HashFunction& hash_function,
                                 size_t size,
                                 std::vector<Element<T> >& hash_values) {
        hash_values.assign(size, { -1, false });
        for (auto number : numbers) {
            hash_values[hash_function(number)] = {number, true};
        }
    }

    HashFunction hash_function_;
    std::vector<Element<T> > hash_values_;
};

template <typename T, class HashFunction>
class FixedSet {
public:
    FixedSet() {}

    void Initialize(const std::vector<T>& numbers) {
        std::mt19937 generator;
        hash_function_ = FindHashFunction(numbers, generator, numbers.size());
        FillHashFunction(numbers, hash_function_, generator, numbers.size(), elements_);
    }

    bool Contains(const T number) const {
        return elements_[hash_function_(number)].Contains(number);
    };

private:
    static HashFunction FindHashFunction(const std::vector<T>& numbers,
                                         std::mt19937& generator,
                                         size_t size) {
        HashFunction hash_function;
        do {
            hash_function = HashFunction::GenerateRandomHashFunction(generator, size);
        } while (!CheckHashCorrectness(numbers, hash_function, size));
        return hash_function;
    }

    static bool CheckHashCorrectness(const std::vector<T>& numbers,
                                     HashFunction& hash_function,
                                     size_t size) {
        std::vector<int> distribution(size, 0);
        for (auto key : numbers) {
            ++distribution[hash_function(key)];
        }
        int64_t distribution_sqr_sum = 0;
        for (auto part_size : distribution) {
            distribution_sqr_sum += part_size * part_size;
        }
        return distribution_sqr_sum <= 4 * numbers.size();
    }

    static void FillHashFunction(const std::vector<int>& numbers,
                                 HashFunction& hash_function,
                                 std::mt19937& generator,
                                 size_t size,
                                 std::vector<InnerHash<T, HashFunction> >& hash_values) {
        std::vector<std::vector<int> > distribution(numbers.size(), std::vector<int>());
        for (auto key : numbers) {
            distribution[hash_function(key)].push_back(key);
        }
        for (auto elements : distribution) {
            hash_values.emplace_back();
            hash_values.back().Initialize(elements, generator);
        }
    }

    HashFunction hash_function_;
    std::vector<InnerHash<T, HashFunction>> elements_;
};

std::vector<int> ReadVector() {
    int elements_number;
    std::cin >> elements_number;
    std::vector<int> elements(elements_number);
    for (size_t i = 0; i < elements_number; ++i) {
        std::cin >> elements[i];
    }
    return elements;
}

std::vector<int> ReadNumbers() {
    return ReadVector();
}

std::vector<int> ReadRequests() {
    return ReadVector();
}

std::vector<bool> ProceedRequests(const FixedSet<int, HashFunction>& set,
                                  const std::vector<int>& requests) {
    std::vector<bool> responds;
    for (auto request : requests) {
        responds.push_back(set.Contains(request));
    }
    return responds;
}

void PrintResponds(const std::vector<bool>& responds) {
    for (auto respond : responds) {
        if (respond) {
            std::cout << "Yes\n";
        } else {
            std::cout << "No\n";
        }
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    std::vector<int> numbers = ReadNumbers();
    std::vector<int> requests = ReadRequests();

    FixedSet<int, HashFunction> set;
    set.Initialize(numbers);

    std::vector<bool> responds = ProceedRequests(set, requests);
    PrintResponds(responds);
}
