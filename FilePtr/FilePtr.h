#pragma once
#include <memory.h>
#include <stdio.h>
#include <memory>
#include <string>

void destroy_unique(FILE* file) {
    fclose(file);
}

void destroy_shared(FILE* file) {
    fclose(file);
}

std::shared_ptr<FILE> open_shared(const std::string& filename, const std::string& mode) {
    FILE* file = fopen(filename.c_str(), mode.c_str());
    if (file == NULL) {
        std::error_code error_code;
        throw (std::system_error(error_code));
    }
    std::shared_ptr<FILE> ptr(file, destroy_shared);
    return ptr;
}

std::unique_ptr<FILE, decltype(&destroy_unique)> open_unique(const std::string& filename, 
                                                             const std::string& mode) {
    FILE* file = fopen(filename.c_str(), mode.c_str());
    if (file == NULL) {
        std::error_code error_code;
        throw (std::system_error(error_code));
    }
    std::unique_ptr<FILE, decltype(&destroy_unique)> ptr(fopen(filename.c_str(), mode.c_str()),
                                                         destroy_unique);
    return ptr;
}
