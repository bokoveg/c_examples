#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
#include <vector>

struct Container {
    explicit Container(int val) {
        val_ = val;
    }

    Container(int val, int index) {
        val_ = val;
        index_ = index;
    }

    int val_;
    int index_;
    int HeapType;
};

class ContainerComparatorMax {
public:
    static bool compare(Container *lhs, Container *rhs) {
        return lhs->val_ > rhs->val_;
    }

    int type = 1;
};

class ContainerComparatorMin {
public:
    static bool compare(Container *lhs, Container *rhs) {
        return lhs->val_ < rhs->val_;
    }

    int type = 2;
};

template<class Comparator>
class Heap {
 public:
    Heap() {
    }

    Container* GetTop() {
        return data_[0];
    }

    void Add(Container* element) {
        data_.push_back(element);
        element->HeapType = comp_.type;
        element->index_ = data_.size() - 1;
        SiftUp(data_.size() - 1);
    }

    void DelTop() {
        DelByIndex(0);
    }

    size_t Size() {
        return data_.size();
    }

    void DelByIndex(int index) {
        if (index != data_.size() - 1) {
            std::swap(data_[index], data_[data_.size() - 1]);
            std::swap(data_[index]->index_, data_[data_.size() - 1]->index_ );
            data_.pop_back();
            SiftDown(index);
            SiftUp(index);
        } else {
            data_.pop_back();
        }
    }

 private:
    std::vector<Container*> data_;
    Comparator comp_;
    void SiftUp(int index) {
        int curNode = index;
        int anc;
        while (curNode > 0) {
            anc = (curNode - 1) / 2;
            if (comp_.compare(data_[curNode], data_[anc])) {
                std::swap(data_[anc], data_[curNode]);
                std::swap(data_[anc]->index_, data_[curNode]->index_);
                curNode = anc;
            } else {
                break;
            }
        }
    }

    void SiftDown(int index) {
        size_t curNode = index;
        while (2 * curNode + 1 < data_.size()) {
            size_t leftSucc = curNode * 2 + 1;
            size_t rightSucc = curNode * 2 + 2;
            size_t minSuccIndex = leftSucc;
            if (rightSucc < data_.size() && comp_.compare(data_[rightSucc], data_[leftSucc])) {
                minSuccIndex = rightSucc;
            }
            if (comp_.compare(data_[curNode], data_[minSuccIndex])) {
                break;
            } else {
                std::swap(data_[minSuccIndex], data_[curNode]);
                std::swap(data_[minSuccIndex]->index_, data_[curNode]->index_);
                curNode = minSuccIndex;
            }
        }
    }
};

size_t ReadInput(std::vector<int> &elements, std::vector<char> &operations) {
    size_t ElementsNumber, OperationsNumber, StatisticOrder;
    std::cin >> ElementsNumber >> OperationsNumber >> StatisticOrder;
    elements.resize(ElementsNumber);
    for (size_t i = 0; i < ElementsNumber; ++i) {
        std::cin >> elements[i];
    }
    operations.resize(OperationsNumber);
    for (size_t i = 0; i < OperationsNumber; ++i) {
        std::cin >> operations[i];
    }
    return StatisticOrder;
}

std::vector<int> Solve(std::vector<int> &elements, 
                       std::vector<char> &operations, 
                       size_t StatisticOrder) {
    size_t Begin = 0, End = 1;

    Heap<ContainerComparatorMax> StatHeap;
    Heap<ContainerComparatorMin> BigHeap;
    
    std::vector<Container*> containers(elements.size());
    Container* first = new Container(elements[Begin]);
    StatHeap.Add(first);
    containers[0] = first;

    std::vector<int> ans;

    for (size_t ind = 0; ind < operations.size(); ++ind) {
        if (operations[ind] == 'L') {
            Container *cont = containers[Begin];
            if (cont->HeapType == 1) {
                StatHeap.DelByIndex(cont->index_);
            } else {
                BigHeap.DelByIndex(cont->index_);
            }
            delete containers[Begin];
            if (BigHeap.Size() > 0 && StatHeap.Size() < StatisticOrder) {
                Container* BigHeapTop = BigHeap.GetTop();
                BigHeap.DelTop();
                StatHeap.Add(BigHeapTop);
            }
            ++Begin;
        } else {
            containers[End] = new Container(elements[End]);
            BigHeap.Add(containers[End]);
            if (StatHeap.Size() < StatisticOrder) {
                Container* BigHeapTop = BigHeap.GetTop();
                BigHeap.DelTop();
                StatHeap.Add(BigHeapTop);
            } else {
                if (BigHeap.GetTop()->val_ < StatHeap.GetTop()->val_) {
                    Container* BigHeapTop = BigHeap.GetTop();
                    Container* StatHeapTop = StatHeap.GetTop();
                    BigHeap.DelTop();
                    StatHeap.DelTop();
                    BigHeap.Add(StatHeapTop);
                    StatHeap.Add(BigHeapTop);
                }
            }
            ++End;
        }
        if (StatHeap.Size() == StatisticOrder) {
            ans.push_back(StatHeap.GetTop()->val_);
        } else {
            ans.push_back(-1);
        }
    }

    while (Begin < End) {
        delete containers[Begin];
        ++Begin;
    }

    return ans;
}

int main() {
    std::vector<int> elements;
    std::vector<char> operations;
    size_t StatisticOrder = ReadInput(elements, operations);
    std::vector<int> ans = Solve(elements, operations, StatisticOrder);
    for (size_t i = 0; i < ans.size(); ++i) {
        std::cout << ans[i] << '\n';
    }
}
