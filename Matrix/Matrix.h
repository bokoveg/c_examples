#pragma once
#include <vector>
#include <iostream>
#include <utility>

class Matrix {
public:
    Matrix(std::vector<std::vector<int> > &elements) {
        if (elements.size() > 0) {
            int length = elements[0].size();
            for (size_t i = 0; i < elements.size(); ++i) {
                if (elements[i].size() != length) {
                    throw std::length_error("constructor row length error");
                }
            }
            elements_ = elements;
            size_ = std::make_pair(elements_.size(), elements_[0].size());
        } else {
            size_ = std::make_pair(0, 0);
        }
    }

    void print() {
        for (size_t i = 0; i < elements_.size(); ++i) {
            for (size_t j = 0; j < elements_[i].size(); ++j) {
                std::cout << elements_[i][j] << '\t';
            }
            std::cout << '\n';
        }
    }

    std::pair<size_t, size_t> size() const {
        return size_;
    }

    int get_elem(int row, int col) const {
        if (row < size_.first && col < size_.second) {
            return elements_[row][col];
        } else {
            throw std::out_of_range("get_elem bad indexes");
        }
    }

    Matrix operator + (const Matrix &other) {
        if (size_ == other.size()) {
            std::vector<std::vector<int> > res(size_.first);
            for (size_t i = 0; i < size_.first; ++i) {
                res[i].resize(size_.second);
                for (size_t j = 0; j < size_.second; ++j) {
                    res[i][j] = get_elem(i, j) + other.get_elem(i, j);
                }
            }
            return Matrix(res);
        } else {
            throw std::length_error("operator+ size error");
        }
    }

private:
    std::vector<std::vector<int> > elements_;
    std::pair<size_t, size_t> size_;
};
