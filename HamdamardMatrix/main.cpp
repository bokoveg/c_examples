#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <algorithm>
#include <vector>

std::vector<bool> CalculateQuadraticResidue(int module) {
    std::vector<bool> answer(module, false);
    for (int i = 0; i < module; ++i) {
        answer[(i * i) % module] = true;
    }
    return answer;
}

void PrintCodes(const std::vector<std::vector<int> > &matrix) {
    for (size_t i = 0; i < matrix.size(); ++i) {
        std::cout << "1";
    }
    std::cout << "\n";
    for (size_t i = 0; i < matrix.size(); ++i) {
        for (int j = 0; j < matrix.size(); ++j) {
            std::cout << (matrix[i][j] == 1 ? 1 : 0);
        }
        std::cout << "\n";
    }
}

int main() {
    freopen("output.txt", "w", stdout);
    int p;
    std::cin >> p;
    std::vector<std::vector<int> > answer(p, std::vector<int>(p));
    std::vector<bool> is_quadratic_residue = CalculateQuadraticResidue(p);
    for (int i = 0; i < p; ++i) {
        for (int j = 0; j < p; ++j) {
            auto element = i - j;
            if (element < 0) {
                element += p;
            }
            if (i == j) {
                answer[i][j] = -1;
            } else {
                if (is_quadratic_residue[element]) {
                    answer[i][j] = 1;
                } else {
                    answer[i][j] = -1;
                }
            }
        }
    }
    PrintCodes(answer);
}
