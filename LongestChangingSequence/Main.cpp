#include <algorithm>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include "StressTester.h"
void ReadInput(std::vector<int> &elements) {
    int elementNumber;
    std::cin >> elementNumber;
    elements.resize(elementNumber);
    for (int i = 0; i < elementNumber; ++i) {
        std::cin >> elements[i];
    }
}
void Solve(std::vector<int> &elements, std::vector<int> &answer) {
    std::reverse(elements.begin(), elements.end());
    std::vector<int> small(elements.size()), big(elements.size());
    std::vector<int> prevSmall(elements.size()), prevBig(elements.size());
    for (int it = 0; it < elements.size(); ++it) {
        small[it] = big[it] = 1;
        prevSmall[it] = prevBig[it] = -1;
        for (int j = 0; j < it; ++j) {
            if (elements[it] < elements[j] && big[j] + 1 > small[it]) {
                small[it] = big[j] + 1;
                prevSmall[it] = j;
            }
            if (elements[it] > elements[j] && small[j] + 1 > big[it]) {
                big[it] = small[j] + 1;
                prevBig[it] = j;
            }
        }
    }
    int curMax = 0;
    for (int i = 0; i < elements.size(); ++i) {
        if (big[i] > curMax) {
            curMax = big[i];
        }
        if (small[i] > curMax) {
            curMax = small[i];
        }
    }
    int lookingForValue = curMax;
    int prevElem;
    int lookingForType = 0;
    for (int it = elements.size() - 1; it >= 0; --it) {
        if (lookingForType == 0) {
            if (big[it] == lookingForValue) {
                answer.push_back(elements[it]);
                prevElem = elements[it];
                lookingForType = -1;
                --lookingForValue;
            }
            else
            if (small[it] == lookingForValue) {
                answer.push_back(elements[it]);
                prevElem = elements[it];
                lookingForType = 1;
                --lookingForValue;
            }
        } 
        else
        if (lookingForType == 1) {
            if (big[it] == lookingForValue && elements[it] > prevElem) {
                answer.push_back(elements[it]);
                prevElem = elements[it];
                lookingForType = -1;
                --lookingForValue;
            }
        } 
        else 
        if (lookingForType == -1) {
            if (small[it] == lookingForValue && elements[it] < prevElem) {
                answer.push_back(elements[it]);
                prevElem = elements[it];
                lookingForType = 1;
                --lookingForValue;
            }
        }
    }
    std::reverse(elements.begin(), elements.end());
}
void PrintAnswer(std::vector<int> &answer) {
    for (int i = 0; i < answer.size(); ++i) {
        std::cout << answer[i] << ' ';
    }
}
int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::vector<int> elements;
    std::vector<int> answer;
/*    ReadInput(elements);
    Solve(elements, answer);
    PrintAnswer(answer);*/
    
    int testNum = 1000;
    for (int i = 0; i < testNum; ++i) {
        std::vector<int> solveAnswer, brutforcAnswer;
        GenerateTest(elements, 10, 20, 1000000);
        Solve(elements, solveAnswer);
        BrutforceSolve(elements, brutforcAnswer);
        Checker(solveAnswer, brutforcAnswer, elements);
        std::cout << "Test " << i + 1 << " passed. \n";
    }
}
