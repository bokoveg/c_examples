#pragma once
#include <algorithm>
#include <vector>
#include <ctime>
#include <string>
void GenerateTest(std::vector<int> &elements, int minElementNumber, 
                  int maxElementNumber, int maxElementValue) {
    srand(time(NULL));
    int n = rand() % (maxElementNumber - minElementNumber) + minElementNumber;
    elements.resize(n);
    for (int i = 0; i < n; ++i) {
        elements[i] = rand() % maxElementValue;
    }
}
std::string CountMask(int x, int length) {
    std::string s;
    for (int i = 0; i < length; ++i) {
        s += '0';
    }
    int pos = 0;
    while (x > 0) {
        s[pos] = '0' + x % 2;
        x /= 2;
        pos++;
    }
    return s;
}
void GenerateMasks(std::vector<std::string> &masks, int maskLength) {
    for (int i = 0; i < 1 << maskLength; ++i) {
        masks.push_back(CountMask(i, maskLength));
    }
}
bool CheckCorrectness(std::vector<int> pretendent) {
    if (pretendent.size() < 2) {
        return true;
    } else {
        bool firstIsLower = (pretendent[0] < pretendent[1]);
        for (int i = 0; i < pretendent.size() - 1; ++i) {
            if ((pretendent[i] < pretendent[i + 1]) == firstIsLower && 
                pretendent[i] != pretendent[i+1]) {
                firstIsLower = 1 - firstIsLower;
            } else {
                return false;
            }
        }
        return true;
    }
}
void BrutforceSolve(std::vector<int> &elements, std::vector<int> &answer) {
    std::vector<std::string> masks;
    GenerateMasks(masks, elements.size());
    sort(masks.begin(), masks.end());
    for (int i = 0; i < masks.size(); ++i) {
        std::vector<int> answerPretendent;
        for (int j = 0; j < masks[i].length(); ++j) {
            if (masks[i][j] == '0') {
                answerPretendent.push_back(elements[j]);
            }
        }
        if (CheckCorrectness(answerPretendent)) {
            if (answerPretendent.size() > answer.size()) {
                answer = answerPretendent;
            }
        }
    }
}
void PrintVector(std::vector<int> &answer) {
    for (int i = 0; i < answer.size(); ++i) {
        std::cout << answer[i] << ' ';
    }
}
void Checker(std::vector<int> solveAnswer,
             std::vector<int> bustAnswer,
             std::vector<int> elements) {
    if (solveAnswer != bustAnswer) {
        std::cout << "Found Error:\n";
        std::cout << " solveAnswer is:\n";
        PrintVector(solveAnswer);
        std::cout << "\n bustAnswer is:\n";
        PrintVector(solveAnswer);
        std::cout << "\n Test is:\n " << elements.size() << '\n';
        PrintVector(elements);
        exit(0);
    } 
}

