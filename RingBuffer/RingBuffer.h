#pragma once

#include <vector>

class RingBuffer {
 public:
    explicit RingBuffer(size_t capacity) {
        storage.resize(capacity);
        this->capacity = capacity;
        begin = 0;
        end = 0;
        size = 0;
    }

    size_t Size() const {
        return size;
    }

    bool Empty() const {
        return size == 0;
    }

    bool TryPush(int element) {
        if (size == storage.capacity()) {
            return false;
        } else {
            storage[begin] = element;
            begin = (begin + 1) % storage.capacity();
            ++size;
            return true;
        }
    }

    bool TryPop(int* element) {
        if (size == 0) {
            return false;
        } else {
            *element = storage[end];
            end = (end + 1) % storage.capacity();
            --size;
            return true;
        }
    }

 private:
    std::vector<int> storage;
    int capacity;
    int size;
    int begin;
    int end;
};
