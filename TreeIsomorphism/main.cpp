#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <utility>

int FindFarest(std::vector<std::vector<int> > &graph, int ind) {
    std::queue<int> BfsQueue;
    std::vector<int> distance;
    for (int i = 0; i < graph.size(); ++i) {
        distance.push_back(-1);
    }
    BfsQueue.push(ind);
    distance[ind] = 0;
    int cur;
    while (BfsQueue.size() > 0) {
        cur = BfsQueue.front();
        BfsQueue.pop();
        for (int i = 0; i < graph[cur].size(); ++i) {
            if (distance[graph[cur][i]] < 0) {
                distance[graph[cur][i]] = distance[cur] + 1;
                BfsQueue.push(graph[cur][i]);
            }
        }
    }
    return cur;
}

std::vector<int> FindWay(std::vector<std::vector<int> > &graph, int StartVertex, int EndVertex) {
    std::queue<int> BfsQueue;
    std::vector<int> prev, distance;
    for (int i = 0; i < graph.size(); ++i) {
        prev.push_back(-1);
        distance.push_back(-1);
    }
    BfsQueue.push(StartVertex);
    prev[StartVertex] = 0;
    distance[StartVertex] = 0;
    int cur;
    while (BfsQueue.size() > 0) {
        cur = BfsQueue.front();
        BfsQueue.pop();
        for (int i = 0; i < graph[cur].size(); ++i) {
            if (distance[graph[cur][i]] < 0) {
                distance[graph[cur][i]] = distance[cur] + 1;
                prev[graph[cur][i]] = cur;
                BfsQueue.push(graph[cur][i]);
            }
        }
    }
    
    std::vector<int> ans;
    int CurVertex = EndVertex;
    while (CurVertex != StartVertex) {
        ans.push_back(CurVertex);
        CurVertex = prev[CurVertex];
    }
    ans.push_back(CurVertex);
    std::reverse(ans.begin(), ans.end());
    return ans;
}

std::vector<int> FindCenters(std::vector<std::vector<int> > &graph) {
    std::vector<int> res;
    int StartVertex = FindFarest(graph, 0);
    int EndVertex = FindFarest(graph, StartVertex);
    std::vector<int> Way = FindWay(graph, StartVertex, EndVertex);
    if (Way.size() % 2 == 0) {
        res.push_back(Way[Way.size() / 2 - 1]);
        res.push_back(Way[Way.size() / 2]);
    } else {
        res.push_back(Way[Way.size() / 2]);
    }
    return res;
}

std::vector<std::vector<int> > MakeGraphRoot(std::vector<std::vector<int> > &graph, int root) {
    std::vector<std::vector<int> > NewGraph(graph.size());
    std::queue<int> BfsQueue;
    std::vector<int> distance;
    for (int i = 0; i < graph.size(); ++i) {
        distance.push_back(-1);
    }
    BfsQueue.push(root);
    distance[root] = 0;
    int cur;
    while (BfsQueue.size() > 0) {
        cur = BfsQueue.front();
        BfsQueue.pop();
        for (int i = 0; i < graph[cur].size(); ++i) {
            if (distance[graph[cur][i]] < 0) {
                NewGraph[cur].push_back(graph[cur][i]);
                distance[graph[cur][i]] = distance[cur] + 1;
                BfsQueue.push(graph[cur][i]);
            }
        }
    }
    return NewGraph;
}


std::vector<std::vector<int> > FindLevels(std::vector<std::vector<int> > &graph, int root) {
    std::vector<std::vector<int> > levels;
    std::queue<int> BfsQueue;
    std::vector<int> distance;
    for (int i = 0; i < graph.size(); ++i) {
        distance.push_back(-1);
    }
    BfsQueue.push(root);
    distance[root] = 0;
    int cur;
    int CurDistance = 0;
    std::vector<int> CurLevel;
    while (BfsQueue.size() > 0) {
        cur = BfsQueue.front();
        if (distance[cur] > CurDistance) {
            levels.push_back(CurLevel);
            ++CurDistance;
            CurLevel.clear();
        }
        CurLevel.push_back(cur);
        BfsQueue.pop();
        for (int i = 0; i < graph[cur].size(); ++i) {
            if (distance[graph[cur][i]] < 0) {
                distance[graph[cur][i]] = distance[cur] + 1;
                BfsQueue.push(graph[cur][i]);
            }
        }
    }
    levels.push_back(CurLevel);
    return levels;
}

void UpdateRes(std::vector<std::vector<int> > &FirstGraph, int FirstVertex, 
               std::vector<int> &FirstVertexMarks,
               std::vector<std::vector<int> > &SecondGraph, int SecondVertex, 
               std::vector<int> &SecondVertexMarks,
               std::vector<int> &res) {
    std::vector<std::pair<int, int> > FirstSuccMarks, SecondSuccMarks;
    for (int i = 0; i < FirstGraph[FirstVertex].size(); ++i) {
        FirstSuccMarks.push_back(std::make_pair(FirstVertexMarks[FirstGraph[FirstVertex][i]],
            FirstGraph[FirstVertex][i]));
    }
    for (int i = 0; i < SecondGraph[SecondVertex].size(); ++i) {
        SecondSuccMarks.push_back(std::make_pair(SecondVertexMarks[SecondGraph[SecondVertex][i]],
            SecondGraph[SecondVertex][i]));
    }
    sort(FirstSuccMarks.begin(), FirstSuccMarks.end());
    sort(SecondSuccMarks.begin(), SecondSuccMarks.end());
    for (int i = 0; i < FirstSuccMarks.size(); ++i) {
        res[FirstSuccMarks[i].second] = SecondSuccMarks[i].second;
        UpdateRes(FirstGraph, FirstSuccMarks[i].second, FirstVertexMarks,
            SecondGraph, SecondSuccMarks[i].second, SecondVertexMarks,
            res);
    }
}

bool FindRootedIsomorphism(std::vector<std::vector<int> > &First, int FirstRoot,
                           std::vector<std::vector<int> > &Second, int SecondRoot,
                           std::vector<int> &res) {
    std::vector<std::vector<int> > FirstLevels, SecondLevels;
    res.clear();
    
    std::vector<int> FirstVertexMarks(First.size()), SecondVertexMarks(Second.size());
    if (First.size() != Second.size()) {
        return false;
    }
    for (int i = 0; i < FirstVertexMarks.size(); ++i) {
        FirstVertexMarks[i] = -1;
        SecondVertexMarks[i] = -1;
    }
    std::vector<std::vector<int> > FirstGraph = MakeGraphRoot(First, FirstRoot);
    std::vector<std::vector<int> > SecondGraph = MakeGraphRoot(Second, SecondRoot);
    FirstLevels = FindLevels(FirstGraph, FirstRoot);
    SecondLevels = FindLevels(SecondGraph, SecondRoot);
    if (FirstLevels.size() != SecondLevels.size()) {
        return false;
    }
    for (int ind = FirstLevels.size() - 1; ind >= 0; --ind) {
        std::vector<std::pair<std::vector<int>, int> > FirstCurrentLevelMarks,
                                                       SecondCurrentLevelMarks;
        for (int j = 0; j < FirstLevels[ind].size(); ++j) {
            int FirstVertex = FirstLevels[ind][j];
            std::vector<int> FirstMark;
            for (int k = 0; k < FirstGraph[FirstVertex].size(); ++k) {
                FirstMark.push_back(FirstVertexMarks[FirstGraph[FirstVertex][k]]);
            }
            sort(FirstMark.begin(), FirstMark.end());
            FirstCurrentLevelMarks.push_back(std::make_pair(FirstMark, FirstVertex));
        }

        for (int j = 0; j < SecondLevels[ind].size(); ++j) {
            int SecondVertex = SecondLevels[ind][j];
            std::vector<int> SecondMark;
            for (int k = 0; k < SecondGraph[SecondVertex].size(); ++k) {
                SecondMark.push_back(SecondVertexMarks[SecondGraph[SecondVertex][k]]);
            }
            sort(SecondMark.begin(), SecondMark.end());
            SecondCurrentLevelMarks.push_back(std::make_pair(SecondMark, SecondVertex));
        }

        if (FirstCurrentLevelMarks.size() != SecondCurrentLevelMarks.size()) {
            return false;
        }

        sort(FirstCurrentLevelMarks.begin(), FirstCurrentLevelMarks.end());
        sort(SecondCurrentLevelMarks.begin(), SecondCurrentLevelMarks.end());

        int markValue = 1;
        for (int i = 0; i < SecondCurrentLevelMarks.size(); ++i) {
            if (i != 0 && SecondCurrentLevelMarks[i].first != 
                SecondCurrentLevelMarks[i - 1].first) {
                markValue++;
            }
            if (SecondCurrentLevelMarks[i].first != FirstCurrentLevelMarks[i].first) {
                return false;
            }
            FirstVertexMarks[FirstCurrentLevelMarks[i].second] = markValue;
            SecondVertexMarks[SecondCurrentLevelMarks[i].second] = markValue;
        }
    }
    res.resize(FirstGraph.size());
    res[FirstRoot] = SecondRoot;
    UpdateRes(FirstGraph, FirstRoot, FirstVertexMarks,
              SecondGraph, SecondRoot, SecondVertexMarks, 
              res);
    return true;
}


std::vector<int> FindIsomorphism(std::vector<std::vector<int> > &FirstGraph,
                                 std::vector<std::vector<int> > &SecondGraph) {
    
    std::vector<int> res;
    std::vector<int> FirstCenters = FindCenters(FirstGraph);
    std::vector<int> SecondCenters = FindCenters(SecondGraph);
    if (FirstGraph.size() == 1) {
        res.push_back(0);
        return res;
    }
    if (FirstCenters.size() != SecondCenters.size()) {
        return res;
    }
    bool verdict;
    if (FirstCenters.size() == 1) {
        verdict = FindRootedIsomorphism(FirstGraph, FirstCenters[0],
                                        SecondGraph, SecondCenters[0],
                                        res);
    } else {
        verdict = FindRootedIsomorphism(FirstGraph, FirstCenters[0], 
                                        SecondGraph, SecondCenters[0],
                                        res);
        if (verdict) {
            return res;
        }
        verdict = FindRootedIsomorphism(FirstGraph, FirstCenters[0],
                                        SecondGraph, SecondCenters[1],
                                        res);
    }
    return res;
}

void ReadInput(std::vector<std::vector<int> > &First, std::vector<std::vector<int> > &Second) {
    int size;
    std::cin >> size;
    First.resize(size);
    Second.resize(size);

    for (int i = 0; i < size - 1; ++i) {
        int FirstVertex, SecondVertex;
        std::cin >> FirstVertex >> SecondVertex;
        --FirstVertex;
        --SecondVertex;
        First[FirstVertex].push_back(SecondVertex);
        First[SecondVertex].push_back(FirstVertex);
    }

    for (int i = 0; i < size - 1; ++i) {
        int FirstVertex, SecondVertex;
        std::cin >> FirstVertex >> SecondVertex;
        --FirstVertex;
        --SecondVertex;
        Second[FirstVertex].push_back(SecondVertex);
        Second[SecondVertex].push_back(FirstVertex);
    }
}

int main() {
    std::vector<std::vector<int> > FirstGraph, SecondGraph;
    ReadInput(FirstGraph, SecondGraph);
    std::vector<int> Isomorphism = FindIsomorphism(FirstGraph, SecondGraph);
    if (Isomorphism.size() > 0) {
        for (int i = 0; i < Isomorphism.size(); ++i) {
            std::cout << Isomorphism[i] + 1 << '\n';
        }
    } else {
        std::cout << -1;
    }
}
