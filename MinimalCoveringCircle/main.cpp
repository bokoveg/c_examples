#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <algorithm>
#include <utility>

struct point {
    double xCoord;
    double yCoord;
};

template <typename Iterator, class Comparator>
Iterator FindMin(Iterator begin, Iterator end, Comparator Compare) {
    Iterator MinPosition = begin;
    for (auto it = begin; it != end; ++it) {
        if (Compare(*it, *MinPosition)) {
            MinPosition = it;
        }
    }
    return MinPosition;
}

template <typename Iterator, class Comparator>
void InsertionSort(Iterator left,
    Iterator right,
    Comparator Compare) {
    if (std::distance(left, right) == 0) {
        return;
    }
    for (auto it = left; it != right; ++it) {
        std::swap(*it, *FindMin(it, right, Compare));
    }
}

template <typename Iterator, class Comparator>
Iterator Partition(Iterator left,
    Iterator right,
    std::mt19937 &generator,
    Comparator Compare) {
    int size = std::distance(left, right);
    std::uniform_int_distribution<int> dist(0, size - 1);
    int PivotDistance = dist(generator);
    auto pivot = left + PivotDistance;
    auto PivotValue = *pivot;
    auto begin = left, end = right - 1;
    while (std::distance(begin, end) > 0) {
        while (Compare(*begin, PivotValue)) {
            ++begin;
        }
        while (Compare(PivotValue, *end)) {
            --end;
        }
        if (std::distance(begin, end) >= 0) {
            std::swap(*begin, *end);
            if (begin != end && !(Compare(*begin, *end) || Compare(*end, *begin))) {
                int RandomDirection = dist(generator);
                if (RandomDirection % 2 == 0) {
                    ++begin;
                } else {
                    --end;
                }
            }
        }
    }

    return begin;
}

template <typename Iterator, class Comparator>
void QuickSortRandom(Iterator left,
    Iterator right,
    std::mt19937 &generator,
    Comparator Compare) {
    const int kMaxInsertionSize = 3;
    int size = std::distance(left, right);
    if (size <= kMaxInsertionSize) {
        InsertionSort(left, right, Compare);
        return;
    }
    auto mid = Partition(left, right, generator, Compare);
    QuickSortRandom(left, mid, generator, Compare);
    QuickSortRandom(mid + 1, right, generator, Compare);
}

template <typename Iterator, class Comparator>
void QuickSort(Iterator begin,
    Iterator end,
    Comparator Compare) {
    std::mt19937 generator;
    QuickSortRandom(begin, end, generator, Compare);
}

std::vector<std::pair<double, bool> > GenerateIntersections(
      std::vector<point> points, 
      double radius) {
    const double kEps = 0.0000001;
    std::vector<std::pair<double, bool> > intersections;
    for (size_t i = 0; i < points.size(); ++i) {
        if (fabs(points[i].yCoord) < radius + kEps) {
            double LeftInter = points[i].xCoord - sqrt(radius * radius -
                points[i].yCoord * points[i].yCoord);
            double RightInter = points[i].xCoord + sqrt(radius * radius -
                points[i].yCoord * points[i].yCoord);
            intersections.push_back(std::make_pair(LeftInter, false));
            intersections.push_back(std::make_pair(RightInter, true));
        }
    }
    return intersections;
}

bool Compare(std::pair<double, bool> &lhs, std::pair<double, bool> &rhs) {
    return lhs < rhs;
}

double Solve(std::vector<point> &points, int CircleNumber) {
    double right = 2000;
    double left = 0;
    const double precise = 0.0001;
    while (fabs(right - left) > precise) {
        double radius = (right + left) / 2;
        std::vector<std::pair<double, bool> > intersections;
        intersections = GenerateIntersections(points, radius);
        QuickSort(intersections.begin(), intersections.end(), Compare);
        int CurCircleNumber = 0;
        bool RadiusIsEnough = false;
        for (size_t i = 0; i < intersections.size(); ++i) {
            if (intersections[i].second) {
                CurCircleNumber--;
            } else {
                CurCircleNumber++;
            }
            if (CurCircleNumber == CircleNumber) {
                RadiusIsEnough = true;
            }
        }
        if (RadiusIsEnough) {
            right = radius;
        } else {
            left = radius;
        }
    }
    return (left + right) / 2;
}

int ReadInput(std::vector<point> &points) {
    int CirclesToCover;
    size_t PointsNum;
    std::cin >> PointsNum >> CirclesToCover;
    
    points.clear();
    points.resize(PointsNum);
    for (size_t i = 0; i < PointsNum; ++i) {
        std::cin >> points[i].xCoord >> points[i].yCoord;
    }
    return CirclesToCover;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::cin.precision(6);

    std::vector<point> points;
    int CircleNum = ReadInput(points);
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::cout << Solve(points, CircleNum);
}
