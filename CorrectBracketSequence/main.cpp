#include <iostream>
#include <stack>
#include <string>

std::string Solve(std::string &line) {
    std::stack<char> stack;
    for (int iter = 0; iter < line.length(); ++iter) {
        if (line[iter] == '(' || line[iter] == '{' ||
            line[iter] == '[') {
            stack.push(line[iter]);
        }
        if (line[iter] == ')') {
            if (stack.size() > 0 && stack.top() == '(') {
                stack.pop();
            } else {
                return std::to_string(iter);
            }
        }
        if (line[iter] == ']') {
            if (stack.size() > 0 && stack.top() == '[') {
                stack.pop();
            } else {
                return std::to_string(iter);
            }
        }
        if (line[iter] == '}') {
            if (stack.size() > 0 && stack.top() == '{') {
                stack.pop();
            } else {
                return std::to_string(iter);
            }
        }
    }
    if (stack.size() == 0) {
        return "CORRECT";
    } else {
        return std::to_string(line.length());
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::string line, ans;
    std::cin >> line;
    std::cout << Solve(line);
}

