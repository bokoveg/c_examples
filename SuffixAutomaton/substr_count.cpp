#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <map>

struct State {
    long long length, link;
    bool endpos;
    std::map<char, long long> next;
};

class SuffixAutomaton {
public:
    SuffixAutomaton() {
        last_ = 0;
        states_.push_back({0, -1, false});
        ways_.push_back(0);
        size_ = 1;
    }

    long long CountWays(long long state_index) {
        if (ways_[state_index] == 0) {
            ways_[state_index] = 1;
            for (auto i = states_[state_index].next.begin();
                 i != states_[state_index].next.end();
                 i++) {
                ways_[state_index] += CountWays(i->second);
            }
        }
        return ways_[state_index];
    }

    void Add(char symbol) {
        string_ += symbol;
        long long cur = size_;
        states_.push_back({states_[last_].length, 0, false});
        size_++;
        ways_.push_back(0);
        long long next_class;
        for (next_class = last_;
             next_class != -1 &&
             states_[next_class].next.find(symbol) == states_[next_class].next.end();
             next_class = states_[next_class].link) {
            states_[next_class].next[symbol] = cur;
        }
        if (next_class == -1) {
            states_[cur].link = 0;
        } else {
            long long next_state = states_[next_class].next[symbol];
            if (states_[next_class].length + 1 == states_[next_state].length) {
                states_[cur].link = next_state;
            } else {
                states_.push_back({states_[next_class].length + 1,
                                   states_[next_state].link,
                                   false,
                                   states_[next_state].next});
                size_++;
                ways_.push_back(0);
                while (next_class != -1 && states_[next_class].next[symbol] == next_state) {
                    states_[next_class].next[symbol] = size_ - 1;
                    next_class = states_[next_class].link;
                }
                states_[next_state].link = size_ - 1;
                states_[cur].link = size_ - 1;
            }
        }
        last_ = cur;
    }

    void Print() {
        std::cout << states_.size() << '\n';
        for (size_t i = 0; i < states_.size(); ++i) {
            std::cout << i << '\n';
            for (auto j = states_[i].next.begin();
                    j != states_[i].next.end(); ++j) {
                std::cout << j->first << ' ' << j->second << '\n';
            }
        }
    }


private:
    long long last_, size_;
    std::vector<State> states_;
    std::string string_;
    std::vector<long long> ways_;
};

int main() {
    std::string line;
    std::cin >> line;
    SuffixAutomaton suffix_automaton;
    for (auto symbol : line) {
        suffix_automaton.Add(symbol);
    }
    std::cout << suffix_automaton.CountWays(0) - 1;
}
