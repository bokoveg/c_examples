#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <memory>

class RMQ {
private:
    std::vector<int> data_;
    std::vector<bool> values_;

public:
    explicit RMQ(size_t size) {
        values_.resize(size * 4 + 1);
        std::fill(values_.begin(), values_.end(), true);
        data_.resize(size * 4 + 1);
        std::fill(data_.begin(), data_.end(), -2);
    }
    void build(std::vector<bool> &values, int node, int left, int right) {

        if (left == right) {
            values_[node] = values[left];
            if (values_[node]) {
                data_[node] = -1;
            } else {
                data_[node] = node;
            }
        } else {
            int mid = (left + right) / 2;
            build(values, node * 2, left, mid);
            build(values, node * 2 + 1, mid + 1, right);
            if (data_[node * 2] < 0) {
                data_[node] = data_[node * 2 + 1];
            } else {
                data_[node] = data_[node * 2];
            }
        }
    }

    void Update(int node, int left_bound, int right_bound, size_t pos, bool value) {
        if (left_bound == right_bound) {
            values_[node] = value;
            if (value) {
                data_[node] = -1;
            } else {
                data_[node] = node;
            }
        } else {
            int mid = (left_bound + right_bound) / 2;
            if (pos <= mid) {
                Update(node * 2, left_bound, mid, pos, value);
            } else {
                Update(node * 2 + 1, mid + 1, right_bound, pos, value);
            }
            if (data_[node * 2] < 0) {
                data_[node] = data_[node * 2 + 1];
            } else {
                data_[node] = data_[node * 2];
            }
        }
    }

    int Get(size_t pos) {
        return values_[pos];
    }

    int GetRmq(int node, int left_bound, int right_bound, int left, int right) {
        if (right < left) {
            return -1;
        }
        if (left_bound == left && right_bound == right) {
            if (data_[node] != -2) {
                return data_[node];
            } else {
                return -1;
            }
        }
        int mid = (left_bound + right_bound) / 2;
        auto left_res = GetRmq(node * 2, left_bound, mid, left, std::min(right, mid));
        auto right_res = GetRmq(node * 2 + 1, mid + 1, right_bound, 
                                std::max(left, mid + 1), right);
        if (left_res < 0) {
            return right_res;
        } else {
            return left_res;
        }
    }
};

int main() {
    int size, request_number;
    std::cin >> size >> request_number;
    std::vector<bool> park(size);
    int fake_size = 1;
    while (fake_size < size) {
        fake_size *= 2;
    }
    park.resize(fake_size);
    for (int i = 0; i < fake_size; ++i) {
        if (i < size) {
            park[i] = false;
        } else {
            park[i] = true;
        }
    }
    size = fake_size;
    RMQ rmq(size);
    rmq.build(park, 1, 0, size - 1);
    for (size_t it = 0; it < request_number; ++it) {
        char symb;
        std::cin >> symb;
        int pos;
        std::cin >> pos;
        pos--;
        if (symb == '+') {
            auto first_free_position = rmq.GetRmq(1, 0, size - 1, pos, pos);
            if (first_free_position != -1) {
                std::cout << pos + 1 << '\n';
                rmq.Update(1, 0, size - 1, pos, true);
                continue;
            }
            first_free_position = rmq.GetRmq(1, 0, size - 1, pos + 1, size - 1);
            if (first_free_position != -1) {
                std::cout << first_free_position - size + 1 << '\n';
                rmq.Update(1, 0, size - 1, first_free_position - size, true);
                continue;
            }
            first_free_position = rmq.GetRmq(1, 0, size - 1, 0, pos - 1);
            if (first_free_position != -1) {
                std::cout << first_free_position - size + 1 << '\n';
                rmq.Update(1, 0, size - 1, first_free_position - size, true);
                continue;
            }
            std::cout << "-1\n";
        } else {
            if (rmq.GetRmq(1, 0, size - 1, pos, pos) != -1) {
                std::cout << "-2\n";
            } else {
                rmq.Update(1, 0, size - 1, pos, false);
                std::cout << "0\n";
            }
        }
    }
}
